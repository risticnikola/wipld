<?php
/**
 *
 * Template Name: Applications - Single
 *
 */

get_header(); ?>

<!-- start the loop -->
<?php while(have_posts()) : the_post();
	/*
	 * Paginate Advanced Custom Field repeater
	 */

	if(get_query_var('page')) {
		$page = get_query_var('page');
	} else {
		$page = 1;
	}

	// Variables
	$row            = 0;
	$items_per_page = 9; // How many images to display on each page
	$items          = get_field('apps_repeater');
	$total          = count($items);
	$pages          = ceil($total / $items_per_page);
	$min            = (($page * $items_per_page) - $items_per_page) + 1;
	$max            = ($min + $items_per_page) - 1;

	?>

    <div class="applicationsArchive">
        <div class="container">
            <div class="row">

                <div class="col-md-3">
					<?php get_template_part('sidebar') ?>
                </div> <!-- /.col-md-3 -->

                <div class="col-md-9">
                    <!-- <h3 class="applicationsArchive__Title"><?php the_title(); ?></h3> -->
                    <div class="row">

						<?php
						$i = 1;
						// check if the repeater field has rows of data
						if(have_rows('apps_repeater')):

							// loop through the rows of data
							while(have_rows('apps_repeater')) : the_row();
								$row ++;
								// Ignore this image if $row is lower than $min
								if($row < $min) {
									continue;
								}
								// Stop loop completely if $row is higher than $max
								if($row > $max) {
									break;
								}
								$link = get_sub_field('pdf');
								?>

                                <div class="col-md-4 col-sm-6">
                                    <div class="applicationsArchive__Box">
                                        <div class="applicationsArchive__Box__Img">
                                            <a href="<?php echo $link; ?>" target="_blank">
                                                <img src="<?php the_sub_field('image'); ?>">
                                            </a>
                                        </div><!-- /.applicationsArchive__Box__Img -->

                                        <div class="applicationsArchive__Box__Content page-content">
											<?php //the_content();  ovde se printa wysiwyg editor ?>

                                            <h5>
                                                <a href="<?php echo $link; ?>" target="_blank">
													<?php the_sub_field('title'); ?>
                                                </a>
                                            </h5>

											<?php the_sub_field('content');

											?>

											<?php //echo do_shortcode("[link link='http://google.com' name='View PDF' size='small']") ?>
                                            <p style="text-align: center">
                                                <a class="shortcodeLink small" href="<?php echo $link; ?>"
                                                   target="_blank">
                                                    View PDF
                                                </a>
                                            </p>

                                        </div><!-- /.applicationsArchive__Box__Content -->

                                    </div> <!-- /.applicationsArchive__Box -->
                                </div><!-- /.col-md-4 -->
								<?php
								if($i % 3 === 0):
									echo '<div class="clearfix"></div>';
								endif;
								$i ++;
							endwhile;

							$paginate_links = paginate_links([
								'base'      => get_permalink() . '%#%' . '/',
								'format'    => '?page=%#%',
								'current'   => $page,
								'total'     => $pages,
								'type'      => 'array',
								'prev_text' => __('<i class="fa fa-angle-left"></i>'),
								'next_text' => __('<i class="fa fa-angle-right"></i>'),
							]);
							if($paginate_links) {
								echo '<div class="clearfix"></div>';
								echo '<div class="Pagination">';
								echo '<ul class="Pagination__Ul">';
								echo '<li class="firstPage"><a href="' . esc_url(get_permalink()) . '"><i class="fa fa-angle-double-left"></i></a></li>';
								foreach($paginate_links as $page) {
									echo '<li>' . $page . '</li>';
								}
								echo '<li class="lastPage"><a href="' . esc_url(get_permalink()) . $pages . '"><i class="fa fa-angle-double-right"></i></a></li>';
								echo '</ul>';
								echo '</div>';
							}
						endif; ?>
                    </div><!-- /.row -->
                </div> <!-- /.col-md-9 -->

            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div><!-- /.applicationsArchive -->

<?php endwhile; ?>
<!-- end the loop -->

<?php get_footer(); ?>
