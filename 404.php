<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package WP_Ogitive
 */

get_header(); ?>




<main>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
<div class="page-content">
    <p>The page you are looking for is not avaiable.

        Please, go back to  <a href='<?php echo home_url(); ?>'><strong>home page</strong></a> or choose another page from navigation.</p>

</div>

            </div>
        </div>
    </div>
</main>

<?php get_footer(); ?>
