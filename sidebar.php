<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package WP_Ogitive
 */
?>
<aside class="App__Sidebar" role="complementary">

	<?php 

	$chk = get_field ('sidebar_options');


	if ($chk == 0 ) { // no sidebar ?>	   

		<!-- no content -->

	<?php } elseif ($chk == 1) { // custom text ?>


			<h3><?php the_field('sidebar_title'); ?></h3>

		    <div class="App__Sidebar__Nav">
		        <?php the_field('sidebar_desc'); ?>
		    </div><!-- /.App__Sidebar__Nav -->


	<?php } elseif ($chk == 2 ) { // navigation import 

		$menuselect = get_field('nav_name');
		$footer_menu_defaults = array (
			'menu'            => $menuselect,
			'container'       => '',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => '',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '<ul>%3$s</ul>',
			'depth'           => 0,
			'walker'          => ''
			);
	?>

			<h3><?php the_field('nav_title'); ?></h3>

		    <div class="App__Sidebar__Nav">
		       <?php
					$footer_menu_defaults['theme_location'] = 'sidebar';
					wp_nav_menu( $footer_menu_defaults );
				?>
		    </div><!-- /.App__Sidebar__Nav -->

	<?php  } elseif ($chk == 3 ) { // auto list child pages ?>

	  <h3><?php echo get_the_title($post->post_parent); ?></h3>

			    <div class="App__Sidebar__Nav">
			        <ul>
						<?php
						$parent = $post->post_parent;
						$args   = [
							'depth'       => 2,
							'title_li'    => '',
							'child_of'    => $parent,
							'sort_column' => 'menu_order'
						];
						//$children = get_pages('child_of=' . $post->ID);
						wp_list_pages($args);
						?>
			        </ul>
			    </div><!-- /.App__Sidebar__Nav --> 
	
	<?php  } elseif ($chk == 4 ) { // news sidebar ?>

	  	<h3>News Categories</h3>

		    <div class="App__Sidebar__Nav">
		        <ul>

		        	<?php $categories = get_categories( array(
					    'orderby' => 'name',
					    'parent'  => 0
					) );
					 
					foreach ( $categories as $category ) {
					    printf( '<li><a href="%1$s">%2$s</a></li>',
					        esc_url( get_category_link( $category->term_id ) ),
					        esc_html( $category->name )
					    );
					} ?>

		        </ul>
		    </div><!-- /.App__Sidebar__Nav --> 


		<h3>Recent News</h3>

		    <div class="App__Sidebar__Nav">
		    	<ul>
		       		<?php 
						// WP_Query arguments
						$args = array (
							'posts_per_page'         => '5',
						);

						// The Query
						$query = new WP_Query( $args );

						// The Loop
						if ( $query->have_posts() ) {
							while ( $query->have_posts() ) {
								$query->the_post(); ?>								

								<li>
									<a href="<?php the_permalink(); ?>">
										<?php the_title(); ?>
									</a>
								</li>

							<?php }
						} else { ?>
							
							No recent news.

						<?php }

						// Restore original Post Data
						wp_reset_postdata();
					?>
				</ul>
		    </div><!-- /.App__Sidebar__Nav --> 


	
	<?php } ?>   


</aside> <!-- /.App__Sidebar -->