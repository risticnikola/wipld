<?php
/**
 *
 * Template Name: Company About Us
 *
 */

get_header(); ?>

<div class="company">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
				<?php get_template_part('sidebar'); ?>
            </div> <!-- /.col-md-3 -->
            <div class="col-md-9">
                <div class="page-content">
					<?php if(have_posts()) : ?>
						<?php while(have_posts()) : the_post();
							the_content();
						endwhile;
					endif; ?>
                </div> <!-- /.page-content -->
            </div> <!-- /.col-md-9 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div><!-- /.company -->

<div class="company__Team">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3 class="title">Team</h3>
                <div class="description page-content">
					<?php the_field('team_text'); ?>
                </div><!-- /.description -->
            </div> <!-- /.col-md-12 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->

	<?php
	// check if the repeater field has rows of data
	if(have_rows('main_team_members')):
		$i = 1;

		// loop through the rows of data
		while(have_rows('main_team_members')) : the_row(); ?>

            <div class="company__Team__Member company__Team__Member--Main <?php if($i % 2 == 1): echo 'gray'; endif; ?>">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="img-holder">
                                <img src="<?php the_sub_field('picture'); ?>"
                                     alt="TeamMember Image">
                            </div><!-- /.img-holder -->
                        </div> <!-- /.col-md-3 -->
                        <div class="col-md-9">
                            <div class="content">
                                <h3><?php the_sub_field('name'); ?></h3>
                                <div class="titleInCompany">
									<?php the_sub_field('title'); ?>
                                </div><!-- /.titleInCompany -->
                                <div class="desc page-content">
									<?php the_sub_field('text'); ?>
                                </div><!-- /.desc -->
                            </div><!-- /.content -->
                        </div> <!-- /.col-md-9 -->
                    </div> <!-- /.row -->
                </div> <!-- /.container -->
            </div><!-- /.company__Team__MainMember -->
			<?php $i ++; ?>
		<?php endwhile;
	endif; ?>

    <div class="Accordion" data-accordion>
        <div class="moreMembers" data-control>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="moreMembers__Btn">
                            <i class="fa fa-angle-down"></i>
                            <span>More Team Members</span>
                        </div><!-- /.moreMembers__Btn -->
                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div><!-- /.moreMembers -->

        <div class="moreMembers__Section" data-content>
            <div class="container">
                <div class="row Flex">
					<?php
					// check if the repeater field has rows of data
					if(have_rows('more_team_members')):

						// loop through the rows of data
						while(have_rows('more_team_members')) : the_row(); ?>
                            <div class="col-md-4 col-xs-6">
                                <div class="company__Team__Member">
                                    <div class="img-holder">
                                        <img src="<?php the_sub_field('picture_m'); ?>"
                                             alt="TeamMember Image">
                                    </div><!-- /.img-holder -->
                                    <h3><?php the_sub_field('name_m'); ?></h3>
                                    <div class="titleInCompany">
										<?php the_sub_field('title_m'); ?>
                                    </div><!-- /.titleInCompany -->
                                    <div class="desc page-content">
										<?php the_sub_field('text_m'); ?>
                                    </div><!-- /.desc -->
                                </div><!-- /.teamMember -->
                            </div><!-- /.col-md-4 -->
						<?php endwhile;
					endif; ?>
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div><!-- /.moreMembers -->
    </div>

</div> <!-- /.company__Team -->


<?php get_footer(); ?>

