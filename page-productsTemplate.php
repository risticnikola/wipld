<?php
/**
 *
 * Template Name: Products - Single
 *
 */

get_header(); ?>
<div class="productsTemplate">
    <div class="container">
        <div class="row">

            <div class="col-md-3">
				<?php get_template_part('sidebar') ?>
            </div> <!-- /.col-md-3 -->

            <div class="col-md-9">
                <div class="page-content">
					<?php if(have_posts()) : ?>
						<?php while(have_posts()) : the_post();
							the_content();
						endwhile;
					endif; ?>
                </div> <!-- /.page-content -->
            </div> <!-- /.col-md-9 -->

        </div> <!-- /.row -->
    </div> <!-- /.container -->
	<?php if(get_field('two_additional_sections') == 'on'): ?>
        <section class="defaultSection defaultSection--Gray ">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="sideBox">

                            <?php if ( get_field('cl_title') ) { ?>
                                <h4 class="bBorderLeft"><?php the_field('cl_title'); ?></h4>
                            <?php } else { ?>
                                <h4 class="bBorderLeft">NEW FEATURES BY</h4>
                            <?php } ?>

                            <div class="page-content">
								<?php the_field('cl_desc'); ?>
                                <a href='<?php the_field('cl_file'); ?>' target="_blank" class="more-info">
									<?php the_field('cl_btntxt'); ?>
                                </a>
                            </div><!-- /.page-content -->

                        </div><!-- /.sideBox -->
                    </div><!-- /.col-md-3 -->
                    <div class="col-md-9">

                        <div class="page-content">
							<?php the_field('cl_lastdesc'); ?>
                            <a href='<?php the_field('cl_lastfile'); ?>' target="_blank" class="more-info">
								<?php the_field('cl_lastbtntxt'); ?>
                            </a>
                        </div><!-- /.page-content -->

                    </div><!-- /.col-md-9 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </section>
        <section class="defaultSection">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="sideBox">

                            <?php if ( get_field('uat_title') ) { ?>
                                <h4 class="bBorderLeft"><?php the_field('uat_title'); ?></h4>
                            <?php } else { ?>
                                <h4 class="bBorderLeft">USEFUL ADD-ON TOOLS</h4>
                            <?php } ?>


                            
                            <div class="page-content">
								<?php the_field('uat_desc'); ?>
                            </div><!-- /.page-content -->
                        </div><!-- /.sideBox -->
                    </div><!-- /.col-md-3 -->
                    <div class="col-md-9">
                        <div class="row">

							<?php
							$chk = get_field('uat_type');
							if($chk == 0) {  // custom content
								// check if the repeater field has rows of data
								if(have_rows('uat_repeater_custom')):
									// loop through the rows of data
									while(have_rows('uat_repeater_custom')) : the_row(); ?>

                                        <div class="col-md-6">
                                            <div class="addonsList">
                                                <h5>
                                                    <a href="<?php the_sub_field('link'); ?>">
														<?php the_sub_field('title'); ?>
                                                    </a>
                                                </h5>
                                                <div>
													<?php the_sub_field('content'); ?>
                                                </div>
                                                <a href="<?php the_sub_field('link'); ?>"
                                                   class="more-info"><?php the_sub_field('button'); ?></a>
                                            </div><!-- /.addonsList -->
                                        </div><!-- /.col-md-6 -->

									<?php endwhile;
								else :
									echo 'No content';
								endif;

							} else { // products objects
								if(have_rows('uat_repeater_object')):
									while(have_rows('uat_repeater_object')) : the_row(); ?>
										<?php
										$post_object = get_sub_field('product');
										if($post_object):
											// override $post
											$post = $post_object;
											setup_postdata($post);
											?>
                                            <div class="col-md-6">
                                                <div class="addonsList">
                                                    <h5>
                                                        <a href="<?php the_permalink(); ?>">
															<?php the_title(); ?>
                                                        </a>
                                                    </h5>
                                                    <div>
														<?php the_excerpt(); ?>
                                                    </div>
                                                    <a href="<?php the_permalink(); ?>" class="more-info">MORE INFO</a>
                                                </div><!-- /.addonsList -->
                                            </div><!-- /.col-md-6 -->
											<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
											?>
										<?php endif; ?>
									<?php endwhile;
								else :
									echo 'No content';
								endif;
							} ?>
                        </div> <!-- /.row -->
                    </div><!-- /.col-md-9 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </section>
	<?php endif; ?>
</div><!-- /.productsTemplateSecond -->

<?php get_footer(); ?>
