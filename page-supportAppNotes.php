<?php
/**
 *
 * Template Name: Support App Notes
 *
 */

get_header(); ?>

<div class="support">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
				<?php get_template_part('sidebar'); ?>
            </div> <!-- /.col-md-3 -->
            <div class="col-md-9">
                <div class="support__Description page-content">
					<?php if(have_posts()) :
						while(have_posts()) :
							the_post();
							the_content();
						endwhile;
					endif; ?>
                </div><!-- /.page-content -->
				<?php if(have_rows('sections_repeater')):
				while(have_rows('sections_repeater')) :
				the_row(); ?>
                <div class="appNotes">
                    <div class="appNotes__Item">
                        <h5><?php the_sub_field('section_name'); ?></h5>
                        <ul>
							<?php if(have_rows('app_note_repeater')):
								while(have_rows('app_note_repeater')) : the_row(); ?>
                                    <li><a href="#"><?php the_sub_field('app_note'); ?></a></li>
								<?php endwhile;
							endif; ?>
                        </ul>
                    </div><!-- /.appNotes__Item -->
					<?php endwhile;
					endif; ?>
                </div><!-- /.appNotes -->
            </div> <!-- /.col-md-9 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div><!-- /.support -->


<?php get_footer(); ?>

