<?php
/**
 * The template for displaying all single posts.
 *
 * @package WP_Ogitive
 */
get_header();
?>

    <!-- start the loop -->
<?php while(have_posts()) : the_post(); ?>


    <section class="Page">
        <div class="container">
            <div class="row">

                <div class="col-md-3">
					<?php get_template_part('sidebar_news'); ?>
                </div><!-- /.col-md-3 -->

                <div class="col-md-9">
                    <div class="page-content-header">
                        <h2><?php the_title(); ?></h2>
                        <p>
                            <span class="date"><?php the_date(); ?></span>
							<?php $location = get_field('location');
							if($location):?>
                                <span class="location"><?php echo $location; ?></span>
							<?php endif; ?>
                        </p>
                    </div><!-- /.page-content-header -->
                    <div class="page-content">
						<?php the_content(); ?>
                    </div>
                    <!-- /.page-content -->
                </div> <!-- /.col-md-9 -->

            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section> <!-- /.Pages -->


<?php endwhile; ?>
    <!-- end the loop -->

<?php get_footer(); ?>