/* global require */

var gulp = require('gulp');
var merge = require('merge-stream');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var minify = require('gulp-minify');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');
var jshint = require('gulp-jshint');
var watch = require('gulp-watch');
var imagemin = require('gulp-imagemin');
var runSequence = require('run-sequence');
var del = require('del');

var src = 'assets/';
var dest = 'dist/';

var sassOptions = {
    errLogToConsole: true,
    outputStyle: 'expanded'
};
var autoprefixerOptions = {
    browsers: [
        'Android >= 3',
        'BlackBerry >= 7',
        'Chrome >= 9',
        'Firefox >= 4',
        'Explorer >= 9',
        'iOS >= 5',
        'Opera >= 11',
        'Safari >= 5',
        'OperaMobile >= 11',
        'OperaMini >= 6',
        'ChromeAndroid >= 9',
        'FirefoxAndroid >= 4',
        'ExplorerMobile >= 9'
    ]
};


gulp.task('css', function () {
    return gulp.src('assets/styles/sass/main.sass')
        .pipe(sourcemaps.init())
        .pipe(sass(sassOptions).on('error', sass.logError))
        .pipe(autoprefixer(autoprefixerOptions))
        .pipe(sourcemaps.write('./maps'))
        //.pipe(minifyCSS())
        .pipe(gulp.dest(dest + 'css'));
});

// Scripts
gulp.task('scripts', ['jshint', 'js']);
gulp.task('jshint', function () {

    return gulp.src([
        src + 'js/main.js'
    ])
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('js', function () {
    return gulp.src([
        './node_modules/jquery/dist/jquery.js',
        './node_modules/object-fit-images/dist/ofi.min.js',
        './node_modules/rellax/rellax.min.js',
        './node_modules/jquery.mmenu/dist/jquery.mmenu.js',
        './node_modules/jquery.mmenu/dist/addons/counters/jquery.mmenu.counters.js',
        './node_modules/swiper/dist/js/swiper.js',
        './node_modules/select2/dist/js/select2.min.js',
        src + 'js/jquery.accordion.js',
        src + 'js/main.js'
    ])
    //.pipe(uglify())
        .pipe(concat('all.min.js'))
        .pipe(gulp.dest(dest + 'js'));
});

// Fonts
gulp.task('fonts', function () {
    return gulp.src([
        src + 'fonts/**/*',
        './node_modules/font-awesome/fonts/**/*'])
        .pipe(gulp.dest(dest + 'fonts'));
});

// Images
gulp.task('images', function () {
    return gulp.src(src + 'images/**/*.+(png|jpg|jpeg|gif|svg)')
        .pipe(imagemin({
            interlaced: true
        }))
        .pipe(gulp.dest(dest + 'images'));
});

// Watch Files For Changes
gulp.task('watch', function () {
    gulp.watch(src + 'js/main.js', ['scripts']);
    gulp.watch(src + 'styles/sass/**/*', ['css']);
});

// ### Clean
// `gulp clean` - Deletes the build folder entirely.
gulp.task('clean', del.bind(null, ['dist']));

gulp.task('build', function (callback) {
    runSequence(
        'css',
        'scripts',
        ['fonts', 'images'],
        callback);
});

// ### Gulp
// `gulp` - Run a complete build. To compile for production run `gulp --production`.
gulp.task('default', ['clean'], function () {
    gulp.start('build');
});
