$ = jQuery.noConflict();
$(document).ready(function () {

    //Object fit images
    $(function () {
        objectFitImages();
    });

    /**
     * Menu handler
     */

    (function () {

        var handler = $('.main-nav');
        if (!handler.length) {
            return;
        }

        handler.mmenu({
            "extensions": [
                "position-front",
                "position-right",
                "pagedim-black"
            ],
            "counters": true
        }, {
            clone: true
        });

    })();

    var menu = $('.main-nav').data('mmenu');

    $('#menu-button').click(function () {
        console.log(menu);
        if (typeof menu !== 'undefined') {
            menu.open();
        }
    });

    function navHandler(nav, sidebarNav) {
        if (sidebarNav) {
            nav.each(function (index) {
                if (nav[index].children.length > 1) {
                    nav[index].children[1].style.left = nav[index].children[0].offsetWidth + 'px';
                }
            });
        }
        nav.on('mouseover', function () {
            $(this).children('ul').stop().addClass('visible');
        });
        nav.on('mouseleave', function () {
            $(this).children('ul').stop().removeClass('visible');
        });
    }

    var menuItem = $('.App__Header__Nav li');

    navHandler(menuItem);


    /**
     * Header fixed
     */

    var nav = $('.App__Header');
    var headerScroll = function () {

        var st = $(window).scrollTop();
        if (st > 0) {
            nav.addClass('Fixed');
        } else {
            nav.removeClass('Fixed');
        }

    };
    (function () {
        headerScroll();
        $(window).scroll(function () {
            headerScroll();
        });
    })();


    /**
     *  Smooth scroll
     */

    (function () {

        var handler = $('a[data-scroll]');
        if (!handler.length) {
            return;
        }

        handler.each(function () {
            $(this).click(function (e) {
                e.preventDefault();

                var target = $($(this).data('scroll'));
                console.log(target);
                var offset = target.offset().top;
                var margin = parseInt(target.css('margin-top').replace('px', ''));
                var padding = parseInt(target.css('padding-top').replace('px', ''));

                $('html,body').animate({scrollTop: (offset - ((margin + padding) / 2) - 60)}, 1000);
            });
        });
    })();


    /**
     *  Accordion hanlder
     */

    (function () {

        var handler = $('.Accordion');
        if (!handler.length) {
            return;
        }

        handler.accordion({
            "transitionSpeed": 400
        });
    })();


    /**
     *  Search
     */

    (function () {

        var handler = $('.searchBtn');
        if (!handler.length) {
            return;
        }

        handler.each(function () {
            $(this).click(function (e) {
                e.preventDefault();
                $(".topSearch").addClass('push');
                console.log('aa');
                e.stopPropagation();
            });
        });


        $('.topSearch').click(function (e) {
            e.stopPropagation();
        });

        $("html, .searchForm-holder .close-btn").click(function () {
            $(".topSearch").removeClass('push');
        });
    })();


    /**
     *  Img Parallax
     */

    (function () {
        if (!$('.rellax').length) {
            return;
        }
        var rellax = new Rellax('.rellax', {
            center: true
        });
    })();


    /**
     *  More members handler
     */

    (function () {

        var handler = $('.page-content iframe');
        if (!handler.length) {
            return;
        }


        handler.each(function () {
            $(this).wrap('<div class="embed-responsive embed-responsive-16by9"></div>');
        });
    })();


    /**
     *  Select2
     */

    (function () {

        var handler = $('.select2');
        if (!handler.length) {
            return;
        }
        handler.each(function () {
            $(this).select2();
        });
    })();


    /**
     *  Swiper Sliders
     */

    (function () {
        var sliders = {
            homeHeaderSlider: '.homeSlider',
            productSlider: '.productSlider'
        };
        (function () {
            /**
             *  Home Slider
             */
            if (!sliders.homeHeaderSlider.length) {
                return;
            }
            var sliderAutoplay = Number($(sliders.homeHeaderSlider).data('autoplay'));
            var slider = new Swiper(sliders.homeHeaderSlider, {
                speed: 400,
                spaceBetween: 100,
                loop: true,
                effect: 'fade',
                autoplay: {
                    delay: sliderAutoplay ? sliderAutoplay * 1000 : 10000,
                    disableOnInteraction: false
                },
                pagination: {
                    el: '.mainSlider-pagination',
                    type: 'bullets',
                    clickable: true
                },
                navigation: {
                    nextEl: '.homeSliderNext',
                    prevEl: '.homeSliderPrev'
                },
                breakpoints: {
                    768: {
                        autoHeight: true
                    }
                }
            });

        })();

        (function () {
            /**
             *  Product Slider
             */
            if (!sliders.productSlider.length) {
                return;
            }

            var slider = new Swiper(sliders.productSlider, {
                slidesPerView: 6,
                spaceBetween: 30,
                navigation: {
                    nextEl: '.productSliderNext',
                    prevEl: '.productSliderPrev'
                },
                pagination: {
                    el: '.productSlider-pagination',
                    type: 'bullets',
                    clickable: true
                },
                breakpoints: {
                    1200: {
                        slidesPerView: 3
                    },
                    600: {
                        slidesPerView: 2
                    },
                    450: {
                        slidesPerView: 1
                    }
                }
            });

        })();
    })();

});