<?php
/**
 *
 * Template Name: User Register
 *
 */

if (!function_exists('af_members')) {
  return;
}

af_members()->processForm();

get_header();
?>

<div class="company">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
				<?php get_template_part('sidebar'); ?>
            </div> <!-- /.col-md-3 -->
            <div class="col-md-9">

              <div class="page-content">
                <?php if(have_posts()) :
                  while(have_posts()) :
                    the_post();
                    the_content();
                  endwhile;
                endif; ?>
              </div>
              <br>

              <?php af_members()->printFormMessages(); ?>

              <?php af_members()->userForm('register'); ?>

            </div> <!-- /.col-md-9 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div><!-- /.company -->

<?php get_footer(); ?>

