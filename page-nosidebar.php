<?php
/**
 *
 * Template Name: No Sidebar
 *
 */

get_header(); ?>

<!-- start the loop -->
<?php while(have_posts()) : the_post(); ?>

<section class="Page">
    <div class="container">
        <div class="row">
            
            <div class="col-md-12">
                
                <div class="page-content">
                    <?php while(have_posts()) : the_post();
                        the_content();
                    endwhile; ?>
                </div> <!-- /.page-content -->
            </div> <!-- /.col-md-9 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</section> <!-- /.Pages -->

<?php endwhile; ?>
<!-- end the loop -->

<?php get_footer(); ?>
