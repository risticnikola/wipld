<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Ogitive
 */

get_header(); ?>


<div class="newsArchive">
    <div class="container">
        <div class="row">

            <div class="col-md-3">
				<?php get_template_part('sidebar_news'); ?>
            </div> <!-- /.col-md-3 -->

            <div class="col-md-9">
				<?php if(have_posts()) : ?>

					<?php /* Start the Loop */ ?>
					<?php while(have_posts()) : the_post(); ?>
						<?php
						foreach((get_the_category()) as $category) {
							$postcat = $category->cat_ID;
							$catname = $category->cat_name;
						}
						?>
                        <div class="newsHolder">

                            <div class="leftSide">
                                <div class="stripe <?php if($catname == 'Software') {
									echo "stripe--cyan";
								} else if($catname == 'Resources') {
									echo "stripe--red";
								} else echo "stripe--yellow" ?>">
                                <span class="type">
                                    <?php echo $catname; ?>

                                </span>
                                    <span class="date"><?php echo get_the_date() ?></span>
                                </div> <!-- /.stripe -->
                                <div class="newsBox__Img">

                                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                                </div><!-- /.newsBox__Img -->
                            </div> <!-- /.leftSide -->
                            <div class="rightSide">
                                <div class="newsBox__Content">
                                    <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                                    <div class="page-content">
										<?php the_excerpt() ?>
                                    </div> <!-- /.page-content -->
                                    <a href="<?php the_permalink(); ?>" class="more-info">More Info</a>
                                </div><!-- /.newsBox__Content -->
                            </div><!-- /.rightSide -->
                        </div> <!-- /.newsHolder -->
					<?php endwhile;
					pagination();
					endif; ?>
            </div> <!-- /.col-md-9 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div><!-- /.support -->


<?php get_sidebar(); ?>
<?php get_footer(); ?>
