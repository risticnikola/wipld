<?php
if(!is_user_logged_in()) {
	return;
}
?>
<!--    Prikazuje se ako je korisnik ulogovan-->
<div class="support__LoggedIn">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3><?php the_field('section_title'); ?></h3>
                <div class="introText page-content">
					<?php the_field('section_text'); ?>
                </div> <!-- /.introText -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
		<?php if(have_rows('download_repeater')): ?>
            <div class="row">
				<?php while(have_rows('download_repeater')): the_row(); ?>
                    <div class="downloadsDemoBox">
                        <div class="col-md-6">
                            <h5 class="sectionTitle">
                                <b>
									<?php echo do_shortcode(get_sub_field('name')); ?>
                                </b>
                            </h5>
                            <div class="page-content">
                            <div><?php echo do_shortcode(get_sub_field('description')); ?></div>
                            </div> <!-- /.page-content -->
                            <?php echo do_shortcode(get_sub_field('file')); ?>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.downloadsDemoBox -->
				<?php endwhile; ?>
            </div> <!-- /.row -->
		<?php endif; ?>
    </div> <!-- /.container -->
</div><!-- /.supportDemo__LoggedIn -->
<!--    Prikazuje se ako je korisnik ulogovan-->