<h4>Lost your password?</h4>
<br>
<div class="editProfile">
    <form action="" method="post">
        <input type="hidden" name="action" value="pwreset">
		<?php wp_nonce_field('af-login', 'af_nonce', false); ?>

        <p>Please enter your email address. You will receive a link to create a new password via email.</p>
        <br>
        <label for="email">Email</label>
        <input type="text" name="email" value="" id="email" required>


        <button type="submit">Get new password</button>
    </form>
</div>