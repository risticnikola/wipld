<h4>Enter new password</h4>
<br>
<div class="editProfile">
    <form action="" method="post">
        <input type="hidden" name="action" value="pwnew">
		<?php wp_nonce_field('af-login', 'af_nonce', false); ?>

        <label for="pass">Password</label>
        <input type="password" name="pass" id="pass" required>

        <label for="pass2">Retype Password</label>
        <input type="password" name="pass2" id="pass2" required>

        <button type="submit">Change password</button>
    </form>
</div>