<?php
if(!is_user_logged_in()) {
	return;
}
?>
<?php function repeater($repeater) {
	if(have_rows($repeater)): ?>
        <div class="files">
			<?php while(have_rows($repeater)): the_row(); ?>
                <div class="files__Box">
                    <h5 class="sectionTitle">
                        <b>
							<?php echo do_shortcode(get_sub_field('name')); ?>
                        </b>
                    </h5>
					<?php echo do_shortcode(get_sub_field('file')); ?>
                </div><!-- /.col-md-6 -->
			<?php endwhile; ?>
        </div>
	<?php endif;
} ?>


<!--    Prikazuje se ako je korisnik ulogovan-->
<div class="support__LoggedIn support__LoggedIn--Tasks">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3><?php the_field('section_title'); ?></h3>
                <div class="introText page-content">
					<?php the_field('section_text'); ?>
                </div> <!-- /.introText -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->

        <div class="row">
            <div class="col-md-12">
                <h2 class="mb_30">Intro Course Training</h2>
            </div> <!-- /.col-md-12 -->
            <div class="col-md-6">
                <h4 class="mb_30">Training tasks for WIPL-D Pro CAD</h4>
				<?php repeater('intro_course_training_pro_cad_repeater') ?>
                <div class="page-content">
					<?php the_field('intro_course_training_pro_cad_description') ?>
                </div><!-- /.page-content -->
            </div><!-- /.col-md-6 -->
            <div class="col-md-6">
                <h4 class="mb_30">Training tasks for WIPL-D Pro</h4>
				<?php repeater('intro_course_training_pro_repeater') ?>
                <div class="page-content">
					<?php the_field('intro_course_training_pro_description') ?>
                </div><!-- /.page-content -->
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->

        <div class="row">
            <div class="col-md-12">
                <h2 class="mb_30 mt_30">Evaluation Short Course</h2>
            </div><!-- /.col-md-12 -->
            <div class="col-md-6">
                <h4 class="mb_30">Training tasks for WIPL-D Pro CAD</h4>
				<?php repeater('evaluation_short_course_pro_cad_repeater') ?>
                <div class="page-content">
					<?php the_field('evaluation_short_course_pro_cad_description') ?>
                </div><!-- /.page-content -->
            </div><!-- /.col-md-6 -->
            <div class="col-md-6">
                <h4 class="mb_30">Training tasks for WIPL-D Pro</h4>
				<?php repeater('evaluation_short_course_pro_repeate') ?>
                <div class="page-content">
					<?php the_field('evaluation_short_course_pro_description') ?>
                </div><!-- /.page-content -->
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->


    </div> <!-- /.container -->
</div><!-- /.supportDemo__LoggedIn -->
<!--    Prikazuje se ako je korisnik ulogovan-->