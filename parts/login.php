<?php
/**
 * Variables imported thru af_members plugin:
 * @var $email
 * @var $return
 */
?>
<div class="editProfile">
    <form action="" method="post">
        <input type="hidden" name="action" value="login">
        <input type="hidden" name="return" value="<?php echo esc_attr($return); ?>">
		<?php wp_nonce_field('af-login', 'af_nonce', false); ?>

        <label for="email">Email</label>
        <input type="text" name="email" value="<?php echo esc_attr($email); ?>" id="email" required>

        <label for="pass">Password</label>
        <input type="password" name="pass" id="pass" required>

        <button type="submit">Login</button>
    </form>

    <br>
    <a href="<?php echo do_shortcode('[af-pwreset-url]'); ?>">Forgot password?</a> |
    <a href="<?php echo do_shortcode('[af-register-url]'); ?>" class="cyanLink">Register new account</a>

</div>