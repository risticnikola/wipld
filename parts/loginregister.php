<?php if (!is_user_logged_in()): ?>

  <div class="loginRegister">
    You need to <a href="<?php echo do_shortcode('[af-register-url]'); ?>">register</a> and <a href="<?php echo do_shortcode('[af-login-url]'); ?>">log in</a> first to download the demo version.
  </div> <!-- /.loginRegister -->

<?php endif; ?>
