<?php
/**
 * Variables imported thru af_members plugin:
 * @var $data
 * @var $countries
 * @var $action
 */
?>
<div class="editProfile">

  <form action="" method="post">
    <input type="hidden" name="action" value="<?php echo esc_attr($data['action']); ?>">
    <?php wp_nonce_field('af-userfields', 'af_nonce', false); ?>

    <label for="fname">First name *</label>
    <input type="text" name="first_name" value="<?php echo esc_attr($data['first_name']); ?>" id="fname" required>

    <label for="lname">Last name *</label>
    <input type="text" name="last_name" value="<?php echo esc_attr($data['last_name']); ?>" id="lname" required>

    <label for="company">Company / Institution*</label>
    <input type="text" name="company" value="<?php echo esc_attr($data['company']); ?>" id="company" required>

    <label for="interest">Field of interest</label>
    <input type="text" name="interest" value="<?php echo esc_attr($data['interest']); ?>" id="interest">

    <label for="address1">Address *</label>
    <input type="text" name="addressline1" value="<?php echo esc_attr($data['addressline1']); ?>" id="address1" required>

    <label for="address2">Address (2nd line)</label>
    <input type="text" name="addressline2" value="<?php echo esc_attr($data['addressline2']); ?>" id="address2">

    <label for="city">City</label>
    <input type="text" name="city" value="<?php echo esc_attr($data['city']); ?>" id="city">

    <label for="state">State / Province</label>
    <input type="text" name="state" value="<?php echo esc_attr($data['state']); ?>" id="state">

    <label for="zcode">ZIP / Postal code</label>
    <input type="text" name="zip" value="<?php echo esc_attr($data['zip']); ?>" id="zcode">

    <label for="country">Country *</label>
    <select name="country" id="country" class="select2" required>
      <option value="" disabled selected></option>
      <?php
      foreach ($countries as $cc => $cname) {
        /** @noinspection HtmlUnknownAttribute */
        printf(
          '<option value="%s" %s>%s</option>',
          esc_attr($cc),
          $data['country'] == $cc ? 'selected' : '',
          esc_html($cname)
        );
      }
      ?>
    </select><!-- /#country -->

    <label for="email">Email *</label>
    <input type="text" name="email" value="<?php echo esc_attr($data['email']); ?>" id="email" required>


    <?php if ($action == 'register'): ?>

      <label for="pass">Password *</label>
      <input type="password" name="pass" id="pass" required>

      <label for="pass2">Retype Password *</label>
      <input type="password" name="pass2" id="pass2" required>

      <input type="checkbox" name="newsletter" value="1" checked id="newsletter" style="width: auto; height: auto">
      <label for="newsletter" style="margin-left: 10px; display: inline-block">I want to receive WIPL-D newsletter</label>


      <!-- Google reCAPTCHA -->
      <script>
        var renderRecaptcha = function() {
          grecaptcha.render('recaptcha_element', {
            'sitekey' : '<?php echo af_members()->rc_sitekey(); ?>'
          });
        };
      </script>
      <div id="recaptcha_element"></div><br><br>
      <script src="https://www.google.com/recaptcha/api.js?onload=renderRecaptcha&render=explicit&hl=en" async defer></script>
      <!-- /Google reCAPTCHA -->

      <button type="submit">Register</button>


    <?php else: ?>

      <input type="checkbox" name="newsletter" value="1" <?php echo $data['newsletter'] ? 'checked' : ''; ?> id="newsletter" style="width: auto; height: auto">
      <label for="newsletter" style="margin-left: 10px; display: inline-block">I want to receive WIPL-D newsletter</label>

      <p>To change your password enter your old password and new one in following fields.</p>
      <br>

      <label for="oldpass">Old Password</label>
      <input type="text" name="oldpass" id="oldpass">

      <label for="pass">Password</label>
      <input type="password" name="pass" id="pass">

      <label for="pass2">Retype Password</label>
      <input type="password" name="pass2" id="pass2">

      <button type="submit">Save</button>

    <?php endif; ?>

  </form>

</div><!-- /.editProfile -->