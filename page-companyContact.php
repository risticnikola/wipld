<?php
/**
 *
 * Template Name: Company Contact
 *
 */

get_header(); ?>

<div class="company">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
				<?php get_template_part('sidebar'); ?>
            </div> <!-- /.col-md-3 -->
            <div class="col-md-9">
                <div class="page-content">
					<?php while(have_posts()) : the_post();
						the_content();
					endwhile; ?>
                </div> <!-- /.page-content -->
            </div> <!-- /.col-md-9 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div><!-- /.company -->

<?php get_footer(); ?>

