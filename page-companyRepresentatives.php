<?php
/**
 *
 * Template Name: Company Representatives
 *
 */

get_header(); ?>

<div class="company">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
				<?php get_template_part('sidebar'); ?>
            </div> <!-- /.col-md-3 -->
            <div class="col-md-9">
                <div class="page-content">
					<?php if(have_posts()) : ?>
						<?php while(have_posts()) : the_post();
							the_content();
						endwhile;
					endif; ?>
                </div> <!-- /.page-content -->

                <div class="row">
                    <!--  Ako treba da postoji logo na boxu dodati klasu "withLogo" -->
					<?php
					// check if the repeater field has rows of data
					if(have_rows('representatives')):

						// loop through the rows of data
						while(have_rows('representatives')) : the_row(); ?>
                            <div class="col-md-6">
								<?php $logo = get_sub_field('logo'); ?>
                                <div class="grayBox">
									<?php if($logo): ?>
                                        <div class="logoHolder">
                                            <img src="<?php echo $logo; ?>" alt="logo">
                                        </div><!-- /.logoHolder -->
									<?php endif; ?>
                                    <p class="country"><?php the_sub_field('location'); ?></p>
                                    <p class="title"><?php the_sub_field('name'); ?></p>
                                    <p class="sales"><?php the_sub_field('description'); ?></p>
                                    <div class="page-content">
                                        <p><?php the_sub_field('contact'); ?></p>
                                    </div><!-- /.page-content -->
                                </div><!-- /.grayBox -->

                            </div><!-- /.col-md-6 -->
						<?php endwhile;
					else :

						// no rows found
					endif;
					?>
                </div><!-- /.row -->
            </div> <!-- /.col-md-9 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div><!-- /.company -->

<?php get_footer(); ?>

