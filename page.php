<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WP_Ogitive
 */

get_header(); ?>

<section class="Page">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
				<?php get_template_part('sidebar'); ?>
            </div><!-- /.col-md-3 -->
            <div class="col-md-9">
                <div class="page-content">
					<?php while(have_posts()) : the_post();
						the_content();
					endwhile; ?>
                </div> <!-- /.page-content -->
            </div> <!-- /.col-md-9 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</section> <!-- /.Pages -->

<?php get_footer(); ?>
