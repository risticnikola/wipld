<?php
/**
 *
 * Template Name: Products - Landing page
 *
 */

get_header(); ?>


<div class="container">
    <div class="row">
		<?php if(have_rows('products_repeater')):
			while(have_rows('products_repeater')) : the_row(); ?>
                <div class="col-md-6">
                    <div class="mainProductBox">
                        <div class="mainProductBox__MainInfo">
							<?php
							$post_object = get_sub_field('product');
							if($post_object):
								// override $post
								$post = $post_object;
								setup_postdata($post);
								$icons = get_field('icon_type'); ?>
                                <div class="img-holder">
									<?php
									if(has_post_thumbnail()) {
										the_post_thumbnail();
									} else { ?>
                                        <div class="<?php if($icons == '0') {
											echo "tag--Round";
										} else {
											echo "tag--Rect";
										} ?>">
                                            <span><a href="<?php the_permalink(); ?>"><?php the_field('initials') ?></a></span>
                                        </div><!-- /.tag__Round -->
									<?php } ?>
                                </div> <!-- /.img-holder -->
                                <h3><?php the_title(); ?></h3>
                                <div class="mainProductBox__Content">
									<?php the_excerpt(); ?>
                                </div> <!-- /.mainProductBox__Content -->
                                <a href="<?php the_permalink(); ?>" class="more-info">More Info</a><!-- /.more-info -->
								<?php wp_reset_postdata();
							endif; ?>
                        </div><!-- /.mainProductBox__MainInfo -->

                        <div class="mainProductBox__AdditionalInfo">
							<?php $desc = get_sub_field('additional_description');
							if($desc): ?>
                                <div class="mainProductBox__AdditionalInfo__Description">
									<?php echo $desc; ?>
                                </div>
							<?php endif; ?>
							<?php if(have_rows('sidelist_repeater')):
								while(have_rows('sidelist_repeater')) : the_row(); ?>
									<?php
									$post_object = get_sub_field('addon');
									if($post_object):
										$post = $post_object;
										setup_postdata($post); ?>
                                        <p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
										<?php wp_reset_postdata();
									endif;
								endwhile;
							endif; ?>
                        </div> <!-- /.mainProductBox__AdditionalInfo -->
                    </div> <!-- /.mainProductBox -->
                </div> <!-- /.col-md-6 -->
			<?php endwhile;
		else :
			echo 'No products';
		endif; ?>
    </div> <!-- /.row -->
</div> <!-- /.container -->

<section class="defaultSection borderTop addonTools__Section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Add-on Tools</h2>
            </div><!-- /.col-md-12 -->
        </div> <!-- /.row -->
        <div class="row flexAddonTools">
			<?php
			if(have_rows('addons_repeater')):
				while(have_rows('addons_repeater')) : the_row(); ?>
					<?php
					$post_object = get_sub_field('addon');
					if($post_object):
						// override $post
						$post = $post_object;
						setup_postdata($post);
						$icons = get_field('icon_type'); ?>
                        <div class="flexAddonTools__BoxHolder">
                            <div class="addonTools__Section__Box">
                                <div class="<?php if($icons == '0') {
									echo "tag--Round";
								} else {
									echo "tag--Rect";
								} ?>">
                                    <span><a href="<?php the_permalink(); ?>"><?php the_field('initials') ?></a></span>
                                </div><!-- /.tag__Round -->
                                <h5>
                                    <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h5>
                                <div>
									<?php the_excerpt(); ?>
                                </div><!-- /.addonToolsBox__Content -->
                                <a href="<?php the_permalink(); ?>" class="more-info">More Info</a><!-- /.more-info -->
                            </div><!-- /.addonToolBox -->
                        </div><!-- /.col-md-2 -->
						<?php wp_reset_postdata();
					endif;
				endwhile;
			endif; ?>
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</section>

<?php get_footer(); ?>
