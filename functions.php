<?php

/**
 * WP_Ogitive functions and definitions
 *
 * @package WP_Ogitive
 */
if(!function_exists('wpog_setup')) :

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function wpog_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on WP_Ogitive, use a find and replace
		 * to change 'wpog' to the name of your theme in all the template files
		 */
		load_theme_textdomain('wpog', get_template_directory() . '/languages');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		//add_theme_support( 'post-thumbnails' );
		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(array(
			'primary' => __('Primary Menu', 'wpog'),
			'sidebar' => __('Sidebar Menu', 'wpog'),
		));

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support('html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		));

		// Set up the WordPress core custom background feature.
		add_theme_support('custom-background', apply_filters('wpog_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		)));
	}

endif; // wpog_setup
add_action('after_setup_theme', 'wpog_setup');

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function wpog_widgets_init() {
	register_sidebar(array(
		'name'          => __('Sidebar', 'wpog'),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	));
}

add_action('widgets_init', 'wpog_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function wpog_scripts() {

	//Css Paths
	$pathCss = get_template_directory() . '/dist/css/main.css';
	$urlCss  = get_template_directory_uri() . '/dist/css/main.css';

	//Js Paths
	$pathJs = get_template_directory() . '/dist/js/all.min.js';
	$urlJs  = get_template_directory_uri() . '/dist/js/all.min.js';

  wp_enqueue_style('wpog-style', get_stylesheet_uri(), [], 1);
	wp_enqueue_style('wpog-mainCss', $urlCss, '', filemtime($pathCss));
	wp_enqueue_style('wpog-customCss', get_template_directory_uri() . '/custom.css');

	wp_enqueue_script('wpog-js', $urlJs, '', filemtime($pathJs), true);
}

add_action('wp_enqueue_scripts', 'wpog_scripts');

/**
 * Google Fonts integration
 */
function child_styles() {
	if(!is_admin()) {

		// register styles
		wp_register_style('googleFont', '//fonts.googleapis.com/css?family=Raleway:400,700|Roboto:400,400i,700,700i,900', array(), false, 'all');
		// enqueue styles
		wp_enqueue_style('googleFont');
	}
}

add_action('wp_enqueue_scripts', 'child_styles');


/**
 * Load Custom Post Types and Taxonomies
 */
require get_template_directory() . '/inc/custom_post_types.php';
//require get_template_directory() . '/inc/custom_taxonomies.php';

/**
 * Aktivacija thumbnailova i velicine
 */
add_theme_support('post-thumbnails');
add_image_size('fullHome', 1920);
add_image_size('circle-size', 270, 270, false);
add_image_size('phone-img-size', 210);

/**
 *
 * Aktivacija THEME OPTIONS kroz ACF
 *
 */
if(function_exists('acf_add_options_page')) {

	$page = acf_add_options_page(array(
		'page_title' => 'Theme General Settings',
		'menu_title' => 'Theme Options',
		'menu_slug'  => 'theme-general-settings',
		'capability' => 'edit_posts',
		'position'   => '999',
		'icon_url'   => 'dashicons-index-card',
		'redirect'   => false
	));

	acf_add_options_sub_page(array(
		'page_title'  => 'Theme Homepage Settings',
		'menu_title'  => 'Home',
		'parent_slug' => 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title'  => 'Theme Footer Settings',
		'menu_title'  => 'Footer',
		'parent_slug' => 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title'  => 'Theme Slider Settings',
		'menu_title'  => 'Slider',
		'parent_slug' => 'theme-general-settings',
	));
}


/**
 *
 * Brisanje nepotrebnih stvari iz titlova arhivnih strana
 *
 */
add_filter('get_the_archive_title', function($title) {

	if(is_category()) {

		$title = single_cat_title('', false);
	} elseif(is_tag()) {

		$title = single_tag_title('', false);
	} elseif(is_archive()) {

		$title = post_type_archive_title('', false);
	} elseif(is_author()) {

		$title = '<span class="vcard">' . get_the_author() . '</span>';
	}

	return $title;
});

/**
 *
 * Sklanjanje emoji koda iz headera svih strana
 *
 */
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');

remove_action('shutdown', 'wp_ob_end_flush_all', 1);

/**
 *
 * Shortcode
 *
 */

// [custom_link link="http://" name="Link Name"]
function link_func($atts) {
	$atts = shortcode_atts(array(
		'link' => '#',
		'name' => 'custom link',
		'size' => 'small',
	), $atts, 'link');

	return '<a class="shortcodeLink ' . $atts['size'] . '" href="' . $atts['link'] . '">  ' . $atts['name'] . '</a>';
}

add_shortcode('link', 'link_func');


/**
 *
 * Pagination
 *
 */
function pagination() {
	?>
    <div class="Pagination">
		<?php
		global $wp_query;
		$big            = 999999999; // need an unlikely integer
		$max            = intval($wp_query->max_num_pages);
		$paginate_links = paginate_links([
			'base'      => \str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
			'format'    => '?paged=%#%',
			'current'   => max(1, get_query_var('paged')),
			'total'     => $wp_query->max_num_pages,
			'type'      => 'array',
			'prev_text' => __('<i class="fa fa-angle-left"></i>'),
			'next_text' => __('<i class="fa fa-angle-right"></i>'),
		]);
		if($paginate_links) {
			echo '<ul class="Pagination__Ul">';
			echo '<li class="firstPage"><a href="' . esc_url(get_pagenum_link(1)) . '"><i class="fa fa-angle-double-left"></i></a></li>';
			foreach($paginate_links as $page) {
				echo '<li>' . $page . '</li>';
			}
			echo '<li class="lastPage"><a href="' . esc_url(get_pagenum_link($max)) . '"><i class="fa fa-angle-double-right"></i></a></li>';
			echo '</ul>';
		}
		?>
    </div>
	<?php
}


/**
 *
 * Google maps api
 *
 */
function my_acf_init() {

	acf_update_setting('google_api_key', 'XXXXXXXX');
}

add_action('acf/init', 'my_acf_init');


/**
 *
 * Aktivacija google map api
 *
 */
function my_acf_google_map_api($api) {

	$api['key'] = 'XXXXXXXX';

	return $api;
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

/**
 *
 * Excerpt Settings
 *
 */

function custom_excerpt_length($length) {
	return 40;
}

add_filter('excerpt_length', 'custom_excerpt_length', 999);


function new_excerpt_more($more) {
	return '...';
}

// ENABLE EXTENSIONS

add_filter('excerpt_more', 'new_excerpt_more');
add_post_type_support('page', 'excerpt');

function my_myme_types($mime_types){
    $mime_types['exe'] = 'application/x-msdownload'; //Adding exe extension
    return $mime_types;
}
add_filter('upload_mimes', 'my_myme_types', 1, 1);