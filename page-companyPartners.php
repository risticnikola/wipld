<?php
/**
 *
 * Template Name: Company Partners & Users
 *
 */

get_header(); ?>

<div class="company">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
				<?php get_template_part('sidebar'); ?>
            </div> <!-- /.col-md-3 -->
            <div class="col-md-9">
                <div class="companyUsers">
                    <h3><?php the_field('title_partners'); ?></h3>
                    <div class="partnersHolder">
                        <?php
                        // check if the repeater field has rows of data
                        if(have_rows('partners')):

                        // loop through the rows of data
                        while(have_rows('partners')) : the_row(); ?>
                        <div class="partnersHolder__Box">
                            <img src="<?php the_sub_field('logo'); ?>"
                                 class="mockupImg-Partners" alt="Partners Logo">
                            <p><?php the_sub_field('title'); ?></p>
                        </div> <!-- /.partnersHolder__Box -->
                        <?php endwhile;
                        else :

                            // no rows found
                        endif;
                        ?>
                    </div><!-- /.partnersHolder -->

                    <h3><?php the_field('title_users'); ?></h3>
                    <div class="usersList">
                        <div class="page-content">
                            <?php if(have_posts()) :
                                while(have_posts()) :
                                    the_post();
                                    the_content();
                                endwhile;
                            endif; ?>
                        </div> <!-- /.page-content -->

                    </div><!-- /.usersList -->
                </div><!-- /.companyUsers -->
            </div> <!-- /.col-md-9 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div><!-- /.company -->

<?php get_footer(); ?>

