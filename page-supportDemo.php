<?php
/**
 *
 * Template Name: Support Demo
 *
 */

get_header(); ?>
<div class="support">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
				<?php get_template_part('sidebar') ?>
            </div> <!-- /.col-md-3 -->
            <div class="col-md-9">
                <div class="support__Description page-content">
					<?php if(have_posts()) :
						while(have_posts()) :
							the_post();
							the_content();
						endwhile;
					endif; ?>
                </div> <!-- /.support__Description -->

				<?php get_template_part('parts/loginregister'); ?>

            </div> <!-- /.col-md-9 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->

	<?php get_template_part('parts/downloadsDemo'); ?>

</div><!-- /.supportDemo -->


<?php get_footer(); ?>
