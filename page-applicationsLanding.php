<?php
/**
 *
 * Template Name: Applications - Landing
 *
 */

get_header(); ?>

<div class="container">
    <div class="page-content">
		<?php
		while(have_posts()):the_post();
			the_content();
		endwhile; ?>
    </div> <!-- /.page-content -->
    <div class="row">
		<?php if(have_rows('applications_boxes')):
			while(have_rows('applications_boxes')):the_row(); ?>
                <div class="col-md-4 col-sm-6">
                    <div class="ApplicationsBox">
                        <div class="ApplicationsBox__Img">
                            <a href="<?php the_sub_field('link'); ?>">
                                <img src="<?php the_sub_field('img'); ?>" alt="Applications image"/>
                            </a>
                        </div><!-- /.ApplicationsBox__Img -->
                        <div class="ApplicationsBox__Title">
                            <h2>
                                <a href="<?php the_sub_field('link'); ?>">
									<?php the_sub_field('title'); ?>
                                </a>
                            </h2>
                        </div><!-- /.ApplicationsBox__Title -->
                    </div><!-- /.ApplicationsBox -->
                </div><!-- /.col-md-4.col-sm-6 -->
			<?php endwhile; endif; ?>
    </div><!-- /.row -->
</div><!-- /.container -->

<?php get_footer(); ?>
