<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WP_Ogitive
 */
?>

</main> <!-- /.App__Main -->
<footer class="App__Footer">
    <img src="<?php echo get_template_directory_uri(); ?>/dist/images/bkg/Footer-Background.jpg"
         srcset="<?php echo get_template_directory_uri(); ?>/dist/images/bkg/Footer-Background@2x.jpg 2x"
         alt="footer-bkg" class="footerBkg">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <a href="/"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo.svg"
                                 class="logo-footer" alt="logo-footer"></a>
                <div class="socLinks">
                    <a href="<?php the_field('linkedin_', 'options'); ?>" target="_blank"><i
                                class="fa fa-linkedin-square"></i></a>
                    <a href="<?php the_field('youtube', 'options'); ?>" target="_blank"><i
                                class="fa fa-youtube-square"></i></a>
                    <img src="<?php the_field('iso_icon', 'options'); ?>">
                </div><!-- /.socLinks -->
            </div> <!-- col-md-2 -->
            <!--            <div class="col-md-3 col-sm-6 col-md-offset-1">-->
            <!--                <div class="footerBox">-->
            <!--                    <h5>certification</h5>-->
            <!--                    <div class="footerBox__Content page-content">-->
            <!--                        <p>--><?php //the_field('iso_text', 'options'); ?><!--</p>-->
            <!--                        <img src="--><?php //the_field('iso_icon', 'options'); ?><!--">-->
            <!--                    </div><!-- /.footerBox__Content -->
            <!--                </div> <!-- /.footerBox -->
            <!--            </div> <!-- /.col-md-3 -->
            <div class="col-md-4 col-sm-6">
                <div class="footerBox">
                    <h5>QUICK LINKS</h5>
                    <div class="footerBox__Content page-content">
                        <ul>
							<?php
							// check if the repeater field has rows of data
							if(have_rows('text_and_link', 'options')):
								$i = 0;
								// loop through the rows of data
								while(have_rows('text_and_link', 'options')) : the_row(); ?>
									<?php

									$i ++;

									if($i > 5) {
										break;
									}

									?>

                                    <li>
                                        <a href="<?php the_sub_field('quick_link') ?>"><?php the_sub_field('quick_text'); ?></a>
                                    </li>

								<?php endwhile;
							else :
								// no rows found
							endif;
							?>
                        </ul>
                    </div><!-- /.footerBox__Content -->
                </div> <!-- /.footerBox -->
            </div> <!-- /.col-md-3 -->
            <div class="col-md-4 col-sm-6">
                <div class="footerBox">
                    <h5>CONTACT INFO</h5>
                    <div class="footerBox__Content page-content">
						<?php the_field('contact', 'options'); ?>
                    </div><!-- /.footerBox__Content -->
                </div> <!-- /.footerBox -->
            </div> <!-- /.col-md-3 -->
        </div> <!-- row -->
        <p class="webmasterText">
            If you encounter any errors on the website, please let us know at
            <a href="mailto:webmaster@wipl-d.com">webmaster@wipl-d.com.</a>
        </p>
    </div> <!-- container -->
</footer>
</div> <!-- /.App -->
<?php wp_footer(); ?>
</body>
</html>
