<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package WP_Ogitive
 */
?>
<aside class="App__Sidebar" role="complementary">

    <h3><a href="/category/news_events/">Recent News</a></h3>

    <div class="App__Sidebar__Nav">
        <ul>
			<?php
			$postID = get_the_ID();
			// WP_Query arguments
			$args = array(
				'posts_per_page' => 5,
				'post__not_in'   => [$postID]
			);

			// The Query
			$query = new WP_Query($args);

			// The Loop
			if($query->have_posts()) {
				while($query->have_posts()) {
					$query->the_post(); ?>

                    <li>
                        <a href="<?php the_permalink(); ?>">
							<?php the_title(); ?>
                        </a>
                    </li>

				<?php }
			} else { ?>

                No recent news.

			<?php }

			// Restore original Post Data
			wp_reset_postdata();
			?>
        </ul>
    </div><!-- /.App__Sidebar__Nav -->


    <h3>News Categories</h3>

    <div class="App__Sidebar__Nav">
        <ul>

			<?php $categories = get_terms([
				'taxonomy' => 'category',
				'orderby'  => 'name',
				'exclude'  => '7'
			]);

			foreach($categories as $category) {
				printf('<li><a href="%1$s">%2$s</a></li>', esc_url(get_category_link($category->term_id)), esc_html($category->name));
			} ?>

        </ul>
    </div><!-- /.App__Sidebar__Nav -->


</aside> <!-- /.App__Sidebar -->