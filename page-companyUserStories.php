<?php
/**
 *
 * Template Name: Company User Stories
 *
 */

get_header(); ?>

<div class="company">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
				<?php get_template_part('sidebar'); ?>
            </div> <!-- /.col-md-3 -->

            <div class="col-md-9">
                <?php
                // check if the repeater field has rows of data
                if(have_rows('user_stories')):

                // loop through the rows of data
                while(have_rows('user_stories')) : the_row(); ?>
                <div class="userStories">

                        <div class="userStories__Item">
                            <div class="quote">
                                <?php the_sub_field('stories_text'); ?>
                            </div> <!-- /.quote -->
                            <div class="author"><?php the_sub_field('name'); ?></div>
                            <div class="title"><?php the_sub_field('title'); ?></div>
                            <div class="companyName"><?php the_sub_field('company_name'); ?></div>
                            <div class="companyName"><?php the_sub_field('country'); ?></div>
                            <div class="author"><?php the_sub_field('name_2'); ?></div>
                            <div class="title"><?php the_sub_field('title_2'); ?></div>
                            <div class="companyName"><?php the_sub_field('company_name_2'); ?></div>
                            <div class="companyName"><?php the_sub_field('country_2'); ?></div>
                        </div><!-- /.userStories__Item -->

                </div> <!-- /.userStories -->
                <?php endwhile;
                else :

                    // no rows found
                endif;
                ?>
            </div> <!-- /.col-md-9 -->

        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div><!-- /.company -->

<?php get_footer(); ?>

