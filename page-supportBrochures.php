<?php
/**
 *
 * Template Name: Support - Brochures
 *
 */

get_header(); ?>

    <!-- start the loop -->
<?php while(have_posts()) : the_post();

	/*
	 * Paginate Advanced Custom Field repeater
	 */

	if(get_query_var('page')) {
		$page = get_query_var('page');
	} else {
		$page = 1;
	}

	// Variables
	$row            = 0;
	$items_per_page = 9; // How many images to display on each page
	$items          = get_field('support_brochures');
	$total          = count($items);
	$pages          = ceil($total / $items_per_page);
	$min            = (($page * $items_per_page) - $items_per_page) + 1;
	$max            = ($min + $items_per_page) - 1;

	?>

    <div class="support">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
					<?php get_template_part('sidebar'); ?>
                </div> <!-- /.col-md-3 -->
                <div class="col-md-9">
                    <div class="row">
						<?php
						// check if the repeater field has rows of data
						if(have_rows('support_brochures')):
							// loop through the rows of data
							while(have_rows('support_brochures')) : the_row();
								$row ++;
								// Ignore this image if $row is lower than $min
								if($row < $min) {
									continue;
								}
								// Stop loop completely if $row is higher than $max
								if($row > $max) {
									break;
								} ?>

                                <div class="col-md-4">
                                    <div class="brochureBox">
                                        <div class="brochureBox__Img">
                                            <img src="<?php the_sub_field('picture'); ?>">
                                        </div><!-- /.brochureBox__Img -->
                                        <h5><?php the_sub_field('title'); ?></h5>
                                        <a href="<?php the_sub_field('pdf'); ?>" target="_blank" class="btnPdf">View PDF</a>
                                    </div><!-- /.brochureBox -->
                                </div><!-- /.col-md-4 -->
							<?php endwhile;

							$paginate_links = paginate_links([
								'base'      => get_permalink() . '%#%' . '/',
								'format'    => '?page=%#%',
								'current'   => $page,
								'total'     => $pages,
								'type'      => 'array',
								'prev_text' => __('<i class="fa fa-angle-left"></i>'),
								'next_text' => __('<i class="fa fa-angle-right"></i>'),
							]);
							if($paginate_links) {
								echo '<div class="clearfix"></div>';
								echo '<div class="Pagination">';
								echo '<ul class="Pagination__Ul">';
								echo '<li class="firstPage"><a href="' . esc_url(get_permalink()) . '"><i class="fa fa-angle-double-left"></i></a></li>';
								foreach($paginate_links as $page) {
									echo '<li>' . $page . '</li>';
								}
								echo '<li class="lastPage"><a href="' . esc_url(get_permalink()) . $pages . '"><i class="fa fa-angle-double-right"></i></a></li>';
								echo '</ul>';
								echo '</div>';
							}
							// no rows found
						endif;
						?>
                    </div><!-- /.row -->
                </div> <!-- /.col-md-9 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div><!-- /.support -->

<?php endwhile; ?>
    <!-- end the loop -->


<?php get_footer(); ?>