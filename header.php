<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package WP_Ogitive
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
		<?php wp_title(''); ?>
		<?php
		if(wp_title('', false)) {
			echo ' | ';
		}
		?>
		<?php bloginfo('name'); ?>
    </title>

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

	<?php wp_head(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-39374361-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-39374361-1');
</script>

</head>

<body <?php body_class(); ?>>

<div class="App">
    <header class="App__Header">
        <div class="HeaderHolder">
            <div class="topSearch">
                <div class="container">
                    <div class="searchForm-holder">
						<?php get_template_part('searchform'); ?>
                        <span class="close-btn">
                       <i class="fa fa-close"></i>
                    </span> <!-- /.close-btn -->
                    </div> <!-- searchForm-holder -->
                </div> <!-- container -->
            </div> <!-- topSearch -->
            <div class="App__Header__Logo">
                <a href="/">
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo.svg"
                         class="logo-navigation" alt="logo">
                </a>
            </div> <!-- /.App__Logo -->
            <div class="App__Header__Nav">
				<?php get_template_part('navigation'); ?>
                <div id="menu-button"><i class="fa fa-bars"></i></div>
                <!-- /#menu-button -->
            </div>
            <div class="App__Header__Right">
                <a href="#" class="searchBtn links"><i class="fa fa-search"></i></a>
				<?php echo do_shortcode('[af-login]'); ?>
                <a href="<?php the_field('linkedin_', 'options'); ?>" class="socialLink socialLink--Linkedin">
                    <i class="fa fa-linkedin"></i>
                </a>
                <a href="<?php the_field('youtube', 'options'); ?>" class="socialLink socialLink--Youtube">
                    <i class="fa fa-youtube"></i>
                </a>
            </div> <!-- /.Header__Right -->
        </div> <!-- /.HeaderHolder -->
    </header>

    <main class="App__Main">
		<?php if(is_home()):
			get_template_part('slider');
		else: ?>
            <div class="pageHeader">
                <img src="<?php echo get_template_directory_uri(); ?>/dist/images/bkg/headerImg-inside_2.jpg"
                     alt="headerImg">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1><?php the_title(); ?></h1>
                        </div><!-- /.col-md-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div> <!-- /.pageHeader -->
		<?php endif; ?>




