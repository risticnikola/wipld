<?php
/**
 *
 * Template Name: News and Events Archive
 *
 */

get_header(); ?>

<div class="newsArchive">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
				<?php get_template_part('sidebar'); ?>
            </div> <!-- /.col-md-3 -->
            <div class="col-md-9">
				<?php for($i = 0; $i < 6; $i ++): ?>
                    <div class="newsHolder">
                        <div class="newsBox__Img">
                            <div class="stripe <?php if($i == 1):
								echo 'stripe--red'; endif; ?> <?php if($i == 0):
								echo 'stripe--cyan'; endif; ?><?php if($i == 2):
								echo 'stripe--yellow';
							else: echo 'stripe--cyan'; endif; ?>">
                                <span class="type">software</span>
                                <span class="date">10.12.2017</span>
                            </div> <!-- /.stripe -->
                            <img src="<?php echo get_template_directory_uri(); ?>/dist/images/dummy/news-mokap1.jpg"
                                 srcset="<?php echo get_template_directory_uri(); ?>/dist/images/dummy/news-mokap1@2x.jpg 2x"
                                 class="news-mokap1" alt="news-img">
                        </div><!-- /.newsBox__Img -->
                        <div class="newsBox__Content">
                            <h5>Title of some news or event that goes in two rows lorem ipsum dolor
                                sit <?php if($i == 1):
									echo 'Title of some news or event that goes in two rows'; endif; ?></h5>
                            <div class="page-content">
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                                aegrotum
                                sum perotum
                            </div> <!-- /.page-content -->
                            <a href="#" class="more-info">More Info</a>
                        </div><!-- /.newsBox__Content -->
                    </div> <!-- /.newsHolder -->
				<?php endfor; ?>
            </div> <!-- /.col-md-9 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div><!-- /.support -->

<?php get_footer(); ?>

