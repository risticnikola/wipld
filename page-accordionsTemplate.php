<?php
/**
 *
 * Template Name: Accordions Template (New)
 *
 */

get_header(); ?>

<div class="support">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php get_template_part('sidebar'); ?>
            </div> <!-- /.col-md-3 -->
            <div class="col-md-9">
                <div class="CustomAccordionSection">
                    <div class="page-content mb_30">
                        <?php if (have_posts()):
                            while (have_posts()): the_post();
                                the_content();
                            endwhile;
                        endif; ?>
                    </div>
                    <div class="Faq CustomAcc">
                        <div data-accordion-group>
                            <?php if (have_rows('accordions-custom')):
                                while (have_rows('accordions-custom')) :
                                    the_row(); ?>
                                    <h3><?php the_sub_field('title'); ?></h3>
                                    <div class="descHolder page-content"><?php the_sub_field('description') ?></div>
                                    <?php $item = get_sub_field('item');
                                    if (!empty($item)): ?>
                                        <div class="Accordion" data-accordion>
                                            <div data-control>Read more</div>
                                            <div data-content>
                                                <div class="page-content">
                                                    <?php echo $item; ?>
                                                </div>
                                            </div> <!-- /.data-content -->
                                        </div> <!-- /.Accordion -->
                                    <?php endif;
                                endwhile;
                            endif; ?>
                        </div> <!-- /[data-accordion-group] -->
                    </div> <!-- /.Faq -->
                </div> <!-- /.CustomAccordionSection -->
            </div> <!-- /.col-md-9 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div><!-- /.support -->

<?php get_footer(); ?>

