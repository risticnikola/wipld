<?php
/**
 *
 * Template Name: User Activate
 *
 */

if (!function_exists('af_members')) {
  return;
}

get_header();
?>

<div class="company">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
				<?php get_template_part('sidebar'); ?>
            </div> <!-- /.col-md-3 -->
            <div class="col-md-9">

              <div class="page-content">
                <?php if(have_posts()) :
                  while(have_posts()) :
                    the_post();
                    the_content();
                  endwhile;
                endif; ?>
              </div>
              <br>

              <?php
              if (af_members()->userActivation()) {
                echo get_field('activation_ok');
              } else {
                echo get_field('activation_error');
              }
              ?>

            </div> <!-- /.col-md-9 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div><!-- /.company -->

<?php get_footer(); ?>

