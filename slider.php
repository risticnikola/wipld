<?php
//Slider for Theme
?>

<div class="homeSlider swiper-container" data-autoplay="<?php the_field('autoplayDuration', 'options'); ?>">
    <div class="swiper-wrapper">
		<?php
		if(have_rows('slajder', 'options')):
			while(have_rows('slajder', 'options')) : the_row(); ?>
                <div class="swiper-slide">
                    <div class="bkgImg">
                        <img src="<?php the_sub_field('pozadinska_slika'); ?>">
                    </div> <!-- /.bkgImg -->

                    <div class="container">
                        <div class="row">
                            <div class="col-sm-5 Relative">
                                <div class="homeSlider__Content">
                                    <h1 class="desktopTitle">
                                        <a href="<?php the_sub_field('link') ?>"><?php the_sub_field('naslov'); ?></a>
                                    </h1>
                                    <h1 class="mobileTitle">
                                        <a href="<?php the_sub_field('link') ?>"><?php the_sub_field('naslov_mobile'); ?></a>
                                    </h1>
                                    <h2><?php the_sub_field('podnaslov'); ?></h2>
                                    <div class="slider-content page-content">
										<?php the_sub_field('tekst'); ?>
                                    </div><!-- /.slider-content page-content -->
                                </div><!-- /.homeSlider__Content -->
                            </div><!-- /.col-md-5 -->
                            <div class="col-sm-7">
                                <div class="slider-img">
                                    <a href="<?php the_sub_field('link') ?>">
                                        <img src="<?php the_sub_field('slika'); ?>" alt="sliderImg"/>
                                    </a>
                                    <div class="swiper-pagination mainSlider-pagination"></div>
                                </div> <!-- /.slider-img -->
                            </div><!-- /.col-md-7 -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div> <!-- /.swiper-slide -->
			<?php endwhile;
		endif; ?>
    </div> <!-- /.swiper-wrapper -->
    <div class="homeSliderPrev swiper-button-prev"></div>
    <div class="homeSliderNext swiper-button-next"></div>
</div> <!-- /.HomeSlider -->
