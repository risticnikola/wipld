<?php
/**
 * The template for displaying search results pages.
 *
 * @package WP_Ogitive
 */

get_header(); ?>

    <section class="SearchSection">
        <div class="container">
            <div class="col-md-9">

				<?php if(have_posts()) : ?>

                    <div class="SearchQuery">
                        <h1 class="page-title"><?php printf(__('Search Results for: %s', 'wpog'), '<span>' . get_search_query() . '</span>'); ?></h1>
                    </div><!-- /.SearchQuery -->


					<?php /* Start the Loop */ ?>
					<?php while(have_posts()) : the_post(); ?>


                        <div class="searchBox">

                            <h2><?php the_title() ?></h2>
                            <div class="page-content">
								<?php the_excerpt() ?>
                            </div> <!-- /.page-content -->
                            <a href="<?php the_permalink() ?>" class="more-info">More Info</a>

                        </div> <!-- /.newsHolder -->


					<?php endwhile; ?>

					<?php pagination(); ?>
				<?php else : ?>

                    <p>There are no results for this search query. Try again with another term or go back to
                        <a href="/">home</a> page.
                    </p>

				<?php endif; ?>
            </div> <!-- /.col-md-9 -->

            <div class="col-md-3">
				<?php get_sidebar(); ?>
            </div> <!-- /.col-md-3 -->


        </div><!-- /.container -->
    </section><!-- /.SearchSection -->
<?php get_footer(); ?>