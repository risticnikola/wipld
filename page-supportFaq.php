<?php
/**
 *
 * Template Name: Support FAQ
 *
 */

get_header(); ?>

<div class="support">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
				<?php get_template_part('sidebar'); ?>
            </div> <!-- /.col-md-3 -->
            <div class="col-md-9">
                <div class="Faq">

                    <div data-accordion-group>
						<?php
						// check if the repeater field has rows of data
						if(have_rows('faq')):
							while(have_rows('faq')) :
								the_row(); ?>
                                <h3><?php the_sub_field('group_title'); ?></h3>
								<?php if(have_rows('faq_questions')):
								while(have_rows('faq_questions')) : the_row(); ?>
                                    <div class="Accordion" data-accordion>
                                        <div data-control><?php the_sub_field('question'); ?></div>
                                        <div data-content>
                                            <div>
												<?php the_sub_field('answer'); ?>
                                            </div>
                                        </div> <!-- /.data-content -->
                                    </div> <!-- /.Accordion -->
								<?php endwhile;
							endif;
							endwhile;
						endif; ?>
                    </div> <!-- /[data-accordion-group] -->
                    <a href="<?php the_field('download_all_faq'); ?>" class="downloadFaq">Download All FAQ</a>
                    <!-- /.downloadFaq -->
                </div> <!-- /.Faq -->
            </div> <!-- /.col-md-9 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->
</div><!-- /.support -->

<?php get_footer(); ?>

