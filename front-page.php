<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

get_header();
?>

    <section class="defaultSectionHome">
        <div class="twoBox">
            <div class="twoBox__Box">
                <div class="twoBox__Box__Img">

                    <a href="<?php the_field('buttonl', 'options'); ?>"><img
                                src="<?php the_field('logol', 'options'); ?>" alt="wipl-d-pro"></a>
                </div> <!-- /.img-holder -->
                <h2 class="twoBox__Box__Title">
                    <a href="<?php the_field('buttonl', 'options'); ?>"><?php the_field('titlel', 'options'); ?></a>
                </h2> <!-- /.twoBox__Box__Title -->
                <div class="twoBox__Box__Content">
					<?php the_field('contentl', 'options'); ?>
                </div> <!-- /.twoBox__Box__Content -->
                <div class="twoBox__Box__Buttons">
                    <a href="<?php the_field('buttonl', 'options'); ?>" class="more-info">More Info</a>
                    <a href="<?php the_field('demo_buttonl', 'options'); ?>" class="getDemo">Get Free Demo</a>
                </div> <!-- /.twoBox__Box__Buttons -->
            </div> <!-- /.twoBox__Box -->

            <div class="twoBox__Box">
                <div class="twoBox__Box__Img">

                    <a href="<?php the_field('buttonr', 'options'); ?>"><img
                                src="<?php the_field('logor', 'options'); ?>" alt="wipl-d-pro"></a>
                </div> <!-- /.img-holder -->
                <h2 class="twoBox__Box__Title">
                    <a href="<?php the_field('buttonr', 'options'); ?>"><?php the_field('titler', 'options'); ?></a>
                </h2> <!-- /.twoBox__Box__Title -->
                <div class="twoBox__Box__Content">
					<?php the_field('contentr', 'options'); ?>
                </div> <!-- /.twoBox__Box__Content -->
                <div class="twoBox__Box__Buttons">
                    <a href="<?php the_field('buttonr', 'options'); ?>" class="more-info">More Info</a>
                    <a href="<?php the_field('demo_buttonr', 'options'); ?>" class="getDemo">Get Free Demo</a>
                </div> <!-- /.twoBox__Box__Buttons -->
            </div> <!-- /.twoBox__Box -->
        </div> <!-- /.twoBox -->
    </section> <!-- /.defaultSection -->


    <section class="defaultSectionHome Parallax">
        <img src="<?php echo get_template_directory_uri(); ?>/dist/images/dummy/home-paralaxImg.jpg" alt="parallax-img"
             srcset="<?php echo get_template_directory_uri(); ?>/dist/images/dummy/home-paralaxImg@2x.jpg 2x"
             class="rellax">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="title"><?php the_field('productslider_title', 'options'); ?></h2>
                    <div class="Description">
						<?php the_field('productslider_desc', 'options'); ?>
                    </div><!-- /.Description -->

                    <div class="productSliderHolder">
                        <div class="productSlider swiper-container">


                            <div class="swiper-wrapper">
								<?php
								if(have_rows('slider_add', 'options')):
									while(have_rows('slider_add', 'options')) : the_row(); ?>
										<?php
										$post_object = get_sub_field('add-on');
										if($post_object):
											// override $post
											$post = $post_object;
											setup_postdata($post);
											$icons = get_field("icon_type");
											?>
                                            <div class="swiper-slide">

                                                <div class="productBox">
                                                    <div class="productBox__Tag <?php if($icons == '0') {
														echo "tag--Round";
													} else {
														echo "tag--Rect";
													} ?>">
                                                        <span><a href="<?php the_permalink(); ?>"><?php the_field('initials'); ?></a></span>
                                                    </div> <!-- /.tag -->
                                                    <div class="productBox__Title">
                                                        <h5>
                                                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                                        </h5>
                                                    </div> <!-- /.title -->

                                                    <div class="productBox__Content">
														<?php the_excerpt(); ?>
                                                    </div> <!-- /.productBox__Content -->
                                                </div><!-- /.productBox -->
                                            </div> <!-- /.swiper-slide -->

											<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
											?>
										<?php endif; ?>
									<?php endwhile;
								else :
									// no rows found
								endif;
								?>
                            </div> <!-- /.swiper-wrapper -->
                        </div> <!-- /.productSlider -->
                        <div class="swiper-pagination productSlider-pagination"></div>
                        <div class="productSliderPrev swiper-button-prev"></div>
                        <div class="productSliderNext swiper-button-next"></div>
                    </div> <!-- /.sliderHolder -->
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section> <!-- /.defaultSection Parallax -->

    <section class="defaultSectionHome newsSection">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2><?php the_field('news_title', 'options') ?></h2>
                    <div class="Description">
						<?php the_field('news_desc', 'options') ?>
                    </div> <!-- /.Description -->
                    <div class="newsHolder">
						<?php
						// the query
						$the_query = new WP_Query(array(
							'category_name'  => 'news_events',
							'posts_per_page' => 3,
						));
						?>

						<?php if($the_query->have_posts()) : ?>
							<?php while($the_query->have_posts()) :
								$the_query->the_post(); ?>
								<?php
								$categoryID   = get_the_category($post)[0]->term_id;
								$categoryName = get_the_category($post)[0]->name;
								$categoryLink = get_category_link($categoryID); ?>
                                <div class="newsBox">
                                    <div class="stripe <?php if($categoryName == 'Software') {
										echo "stripe--cyan";
									} else if($categoryName == 'Resources') {
										echo "stripe--red";
									} else {
										echo "stripe--yellow";
									} ?>">
                                        <a href="<?php echo esc_url($categoryLink); ?>">
                                            <span class="type">
                                                   <?php echo $categoryName; ?>
                                            </span> <!-- /.type -->
                                            <span class="date"><?php echo get_the_date() ?></span>
                                        </a>
                                    </div> <!-- /.stripe -->

                                    <div class="newsBox__Img">
                                        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                                    </div><!-- /.newsBox__Img -->

                                    <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                                    <div class="page-content">
										<?php the_excerpt() ?>
                                    </div> <!-- /.page-content -->
                                    <a href="<?php the_permalink(); ?>" class="more-info">More Info</a>


                                </div> <!-- /.newsBox -->
							<?php endwhile; ?>
							<?php wp_reset_postdata(); ?>

						<?php else : ?>
                            <p><?php __('No News'); ?></p>
						<?php endif; ?>
                    </div> <!-- /.newsHolder -->
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </section> <!-- /.defaultSection -->
<?php
get_footer();